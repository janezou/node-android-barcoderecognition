package com.example.administrator.barcoderecognition;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.barcoderecognition.http.HttpUtils;
import com.xys.libzxing.zxing.activity.CaptureActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Button btn_send;
    private Button btn_scan;
    private TextView textView_scan_result;
    private TextView textView_recipients;
    private TextView textView_telephone;
    private JSONObject getOntherInfo;
    private MainActivity.GetRecognitionTask mGetRecognitionTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView_scan_result = (TextView)findViewById(R.id.textView_scan_result) ;
        textView_recipients = (TextView)findViewById(R.id.textView_recipients) ;
        textView_telephone = (TextView)findViewById(R.id.textView_telephone) ;

        btn_scan = (Button)findViewById(R.id.button_scan);
        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //打开扫描界面扫描条形码或二维码
                Intent openCameraIntent = new Intent(MainActivity.this, CaptureActivity.class);
                startActivityForResult(openCameraIntent, 0);
            }
        });

        btn_send = (Button)findViewById(R.id.button_send);
        btn_send.setEnabled(false);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = textView_telephone.getText().toString();
                String name = textView_recipients.getText().toString();
                String number = textView_scan_result.getText().toString();
                // XXXX，您的单号为xxxx的快件到了，请到东大鹏远公寓取件，取件码为（随机一个四位数）。
                String message = name + "您的单号为" + number + "的快件到了，请到东大鹏远公寓取件，取件码为" + (int)(Math.random()*9000 + 1000);
                sendMessage2(phone, message);
            }
        });
    }

    // 短信发送
    private void sendMessage(String phone, String message){
        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phone, null, message, pi, null);
    }

    private void sendMessage2(String number, String message){
        Uri uri = Uri.parse("smsto:" + number);
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, uri);
        sendIntent.putExtra("sms_body", message);
        startActivity(sendIntent);
    }

    // 二维码识别
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
            scanResult = scanResult.replaceAll(" ","");
            textView_scan_result.setText(scanResult);
            ToastDialog("二维码识别成功，正在向服务端查询数据，请稍后......");
            mGetRecognitionTask = new GetRecognitionTask("192.168.111.9:3001",scanResult);
            mGetRecognitionTask.execute((Void) null);
        }
    }

    // 网络请求
    public class GetRecognitionTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUid;
        private final String mHost;

        private String jsUid;
        private String jsTipMsg;
        private String jsStatus;

        GetRecognitionTask(String host, String Uid) {
            mUid = Uid;
            mHost = host;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            // Simulate network access.
            Map<String,String> params_post = new HashMap<String,String>();
            params_post.put("username", mUid);

            //服务器请求路径
            String strUrlPath = "http://" + mHost +"/login" ;
            String strResult= HttpUtils.submitPostData(strUrlPath, params_post, "utf-8");
            Log.d("http_post",strResult);

            try {
                JSONObject jsStr =  new JSONObject(strResult);
                jsUid = jsStr.getString("uid");
                jsStatus= jsStr.getString("status");
                jsTipMsg= jsStr.getString("tipMsg");

                if(Integer.parseInt(jsStatus) != 0){
                    Log.d("http_post",jsTipMsg + "\n条码ID：" + jsUid);
                    return false;
                }
                getOntherInfo = jsStr.getJSONObject("otherInfo");

            }catch (JSONException e) {
                e.printStackTrace();
                return false;
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mGetRecognitionTask = null;

            if (success) {
                //拉取成功
                updateView();
            }else {
                ToastDialog(jsTipMsg + "\n条码ID：" + jsUid);
            }
        }

        @Override
        protected void onCancelled() {
            mGetRecognitionTask = null;
        }
    }

    // 提示框
    private void ToastDialog(String strMessage) {
        //调用显示Toast对话框
        Toast.makeText(this, strMessage, Toast.LENGTH_LONG).show();
    }

    // 刷新界面
    private void updateView(){
        try {
            textView_telephone.setText(getOntherInfo.getString("recipient_number"));
            textView_recipients.setText(getOntherInfo.getString("recipient_name"));
            btn_send.setEnabled(true);
            Log.d("test","刷新界面成功" );
        }catch (JSONException e){
            e.printStackTrace();
            Log.d("test","刷新界面失败");
        }
    }

}
