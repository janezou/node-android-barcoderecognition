var express = require('express');
var router = express.Router();
var usr = require('dao/dbConnect');

/* GET home page. */
router.get('/', function (req, res) {
    if (req.cookies.islogin) {
        req.session.islogin = req.cookies.islogin;
    }
    if (req.session.islogin) {
        res.locals.islogin = req.session.islogin;
    }
    res.render('index', { title: 'HOME', test: res.locals.islogin });
});


router.route('/login')
    .get(function (req, res) {
        res.render('login', { title: '信息查询', test: res.locals.islogin });
    })
    .post(function (req, res) {
        client = usr.connect();
        result = null;
        console.log(req.body);
        let rst = {};
        const getRst = function (uid, status, tipMsg) {
            return { uid, status, tipMsg }
        }

        usr.selectFun(client, req.body.username,
            function (result) {
                if (result[0] === undefined) {
                    Object.assign(rst, getRst(req.body.username, "1", "单号不存在"))
                } else if (result[0].on_in === 1) {
                    Object.assign(rst, getRst(req.body.username, "1", "该单号短信已发送"))
                } else {
                    Object.assign(rst, getRst(req.body.username, "0", "查询成功"));
                    rst.otherInfo = JSON.parse(JSON.stringify(result[0]));
                }

                console.log(rst);
                res.send(rst);
            }
        );

        usr.updateFun(client, req.body.username,
            function (err) {
                if (err) throw err;
            }
        );
    });

router.get('/logout', function (req, res) {
    res.clearCookie('islogin');
    req.session.destroy();
    res.redirect('/');
});

router.get('/home', function (req, res) {
    if (req.session.islogin) {
        res.locals.islogin = req.session.islogin;
    }
    if (req.cookies.islogin) {
        req.session.islogin = req.cookies.islogin;
    }
    res.render('home', { title: 'Home', user: res.locals.islogin });
});

router.route('/reg')
    .get(function (req, res) {
        res.render('reg', { title: '注册' });
    })
    .post(function (req, res) {
        client = usr.connect();

        usr.insertFun(client, req.body.username, req.body.password2, function (err) {
            if (err) throw err;
            res.send('注册成功');
        });
    });

module.exports = router;

