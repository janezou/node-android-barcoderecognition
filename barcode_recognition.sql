/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : barcode_recognition

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-05-19 11:54:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for send_table
-- ----------------------------
DROP TABLE IF EXISTS `send_table`;
CREATE TABLE `send_table` (
  `send_number` varchar(20) NOT NULL,
  `sender_name` varchar(50) NOT NULL,
  `sender_address` varchar(50) NOT NULL,
  `sender_number` varchar(11) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `recipient_address` varchar(50) NOT NULL,
  `recipient_number` varchar(11) NOT NULL,
  `on_in` int(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`send_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用来进行单据数据的填写，主要包括寄件人姓名，地址，电话和收件人的姓名，地址，电话，还有与外表相连的外键send_id';

-- ----------------------------
-- Records of send_table
-- ----------------------------
INSERT INTO `send_table` VALUES ('1111111111', '梁友超', '山东烟台', '18507499394', '梁超', '秦皇岛', '17727474004', '0');
INSERT INTO `send_table` VALUES ('2222222222', '梁友超', '山东烟台', '18507499394', '梁超', '秦皇岛', '17727474004', '0');
INSERT INTO `send_table` VALUES ('6519303913', '梁友超', '山东烟台', '18507499394', '梁超', '秦皇岛', '17727474004', '0');
